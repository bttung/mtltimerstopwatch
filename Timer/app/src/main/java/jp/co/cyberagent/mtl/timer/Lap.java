package jp.co.cyberagent.mtl.timer;

/**
 * Created by a13561 on 2015/07/08.
 */
public class Lap {

    private long id;
    private String title;
    private String value;

    public Lap(String title, String value, long id) {
        this.title = title;
        this.value = value;
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public String getTitle() {

        return title;
    }

    public long getId() {
        return id;
    }
}
