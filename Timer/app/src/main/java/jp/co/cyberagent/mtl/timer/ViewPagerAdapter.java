package jp.co.cyberagent.mtl.timer;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by a13561 on 2015/07/06.
 */
public class ViewPagerAdapter extends FragmentPagerAdapter {

    private String tabTitles[] = null;
    private Context context;

    public ViewPagerAdapter(FragmentManager fm, Context c) {
        super(fm);
        context = c;
        tabTitles = context.getResources().getStringArray(R.array.tab_titles);
    }


    @Override
    public int getCount() {
        return tabTitles.length;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                StopwatchFragment stopwatchFragment = new StopwatchFragment();
                return stopwatchFragment;
            case 1:
                TimerFragment timerFragment = new TimerFragment();
                return timerFragment;
        }

        return null;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles[position];
    }
}
