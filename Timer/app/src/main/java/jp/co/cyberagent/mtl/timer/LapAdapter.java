package jp.co.cyberagent.mtl.timer;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by a13561 on 2015/07/07.
 */
public class LapAdapter extends BaseAdapter {

    private Context context;
    private LayoutInflater layoutInflater = null;
    private ArrayList<Lap> lapList;

    public LapAdapter(Context context) {
        this.context = context;
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setLapList(ArrayList<Lap> lapList) {
        this.lapList = lapList;
    }

    @Override
    public int getCount() {
        return lapList.size();
    }

    @Override
    public Object getItem(int position) {
        return lapList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return lapList.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = layoutInflater.inflate(R.layout.lap_row, parent, false);

        ((TextView)convertView.findViewById(R.id.lap_row_title)).setText(lapList.get(position).getTitle());
        ((TextView)convertView.findViewById(R.id.lap_row_value)).setText(lapList.get(position).getValue());

        return convertView;
    }
}
