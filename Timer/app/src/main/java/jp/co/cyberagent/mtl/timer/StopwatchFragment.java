package jp.co.cyberagent.mtl.timer;

import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by a13561 on 2015/07/06.
 */
public class StopwatchFragment extends Fragment {

    private Handler watchHandler = new Handler();
    private TimerStatus timerStatus = TimerStatus.INITIAL;
    private long startTime = 0L;
    private long timeInMilliSeconds = 0L;
    private long swapTime = 0L;
    private long lastLapTime = 0L;
    private long lapTime = 0L;
    private long finalTime = 0L;

    private RelativeLayout mainLayout = null;
    private RelativeLayout actionButtonView = null;
    private TextView detailTv = null;
    private Button leftBt = null;
    private Button rightBt = null;

    private ArrayList<Lap> lapList = null;
    private LapAdapter lapAdapter = null;
    private ListView listView = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.stopwatch_fragment, container, false);

        mainLayout = (RelativeLayout) view.findViewById(R.id.stop_watch_layout);
        detailTv = (TextView) view.findViewById(R.id.detail_tv);
        AppendActionButtonLayout(inflater, container, view);

        lapList = new ArrayList<Lap>();
        lapAdapter = new LapAdapter(getActivity());
        lapAdapter.setLapList(lapList);

        listView = (ListView) view.findViewById(R.id.lap_listView);
        listView.setAdapter(lapAdapter);

        return view;
    }

    private void AppendActionButtonLayout(LayoutInflater inflater, ViewGroup container, View view) {
        actionButtonView =
                (RelativeLayout) inflater.inflate(R.layout.action_button_layout, container, false);

        leftBt = (Button) actionButtonView.findViewById(R.id.left_bt);
        rightBt = (Button) actionButtonView.findViewById(R.id.right_bt);
        leftBt.setOnClickListener(leftHandler);
        rightBt.setEnabled(false);

        RelativeLayout.LayoutParams params =
                new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE);
        params.addRule(RelativeLayout.BELOW, detailTv.getId());

        mainLayout.addView(actionButtonView, params);
    }

    private View.OnClickListener leftHandler = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (timerStatus) {
                case INITIAL:
                    timerStatus = TimerStatus.MESEASURING;
                    startTime = SystemClock.uptimeMillis();
                    lastLapTime = SystemClock.uptimeMillis();

                    leftBt.setText(getResources().getString(R.string.stop_bt));
                    rightBt.setText(getResources().getString(R.string.lap_bt));
                    rightBt.setEnabled(true);
                    rightBt.setOnClickListener(rightHandler);

                    watchHandler.postDelayed(updateTimerThread, 0L);
                    break;
                case MESEASURING:
                    timerStatus = TimerStatus.PAUSED;

                    leftBt.setText(getResources().getString(R.string.start_bt));
                    rightBt.setText(getResources().getString(R.string.reset_bt));
                    watchHandler.removeCallbacks(updateTimerThread);
                    break;
                case PAUSED:
                    timerStatus = TimerStatus.MESEASURING;
                    startTime = SystemClock.uptimeMillis();
                    swapTime += timeInMilliSeconds;

                    leftBt.setText(getResources().getString(R.string.stop_bt));
                    rightBt.setText(getResources().getString(R.string.lap_bt));
                    watchHandler.postDelayed(updateTimerThread, 0L);
                    break;
                default:
                    break;
            }
        }
    };

    private View.OnClickListener rightHandler = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (timerStatus) {
                case MESEASURING:
                    lapTime = SystemClock.uptimeMillis() - lastLapTime;
                    String lapTitle = getResources().getString(R.string.lap_row_title) + " " + lapList.size();
                    String lapValue = TimeUtil.timeToString(lapTime);
                    long lapId = (long) lapList.size();
                    Lap lap = new Lap(lapTitle, lapValue, lapId);

                    lapList.add(0, lap);
                    lapAdapter.notifyDataSetChanged();
                    lastLapTime = SystemClock.uptimeMillis();
                    break;
                case PAUSED:
                    // reset timer here
                    timerStatus = TimerStatus.INITIAL;
                    detailTv.setText(getResources().getString(R.string.initial_time));
                    rightBt.setText(getResources().getString(R.string.lap_bt));
                    rightBt.setEnabled(false);

                    lapList.clear();
                    lapAdapter.notifyDataSetChanged();
                    startTime = 0L;
                    swapTime = 0L;
                    watchHandler.removeCallbacks(updateTimerThread);
                    break;
                default:
                    break;
            }
        }
    };

    private Runnable updateTimerThread = new Runnable() {
        @Override
        public void run() {
            timeInMilliSeconds = SystemClock.uptimeMillis() - startTime;
            finalTime = timeInMilliSeconds + swapTime;
            detailTv.setText(TimeUtil.timeToString(finalTime));
            watchHandler.postDelayed(this, 0L);
        }
    };
}
