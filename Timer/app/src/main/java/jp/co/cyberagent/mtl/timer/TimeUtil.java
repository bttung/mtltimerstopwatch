package jp.co.cyberagent.mtl.timer;

/**
 * Created by a13561 on 2015/07/07.
 */
public class TimeUtil {

    public static String timeToString(long timeInMilliSeconds) {
        int secs = (int) (timeInMilliSeconds / 1000);
        int hours = secs / 3600;
        secs = secs % 3600;
        int mins = secs / 60;
        secs = secs % 60;
        int milliSeconds = (int) (timeInMilliSeconds % 1000) / 10;

        String resuls = "";
        if (hours > 0) {
            resuls += (hours + ":");
        }

        resuls += (mins + ":" + String.format("%02d", secs) + "."+ String.format("%02d", milliSeconds));
        return resuls;
    }


    public static String timeToStringInSecond(long timeInMilliSeconds) {
        int secs = (int) (timeInMilliSeconds / 1000);
        int hours = secs / 3600;
        secs = secs % 3600;
        int mins = secs / 60;
        secs = secs % 60;

        String resuls = "";
        if (hours > 0) {
            resuls += (hours + ":");
        }

        resuls += (mins + ":" + String.format("%02d", secs));
        return resuls;
    }
}
