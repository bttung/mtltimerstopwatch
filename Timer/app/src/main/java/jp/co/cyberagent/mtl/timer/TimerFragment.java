package jp.co.cyberagent.mtl.timer;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Created by a13561 on 2015/07/06.
 */
public class TimerFragment extends Fragment {

    private Handler watchHandler = new Handler();
    private long durationTime = 0L;
    private long startTime = 0L;
    private long timeInMilliSeconds = 0L;
    private long swapTime = 0L;
    private long remainTime = 0L;

    private TimerStatus timerStatus = TimerStatus.INITIAL;
    private NumberPicker hourPicker = null;
    private NumberPicker minutePicker = null;
    private NumberPicker secondPicker = null;

    private LinearLayout timerPicker = null;
    private TextView detailTv = null;
    private Button leftBt = null;
    private Button rightBt = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.timer_fragment, container, false);

        SetupTimerPicker(view);
        AppendActionButtonLayout(inflater, container, view);
        detailTv = (TextView) view.findViewById(R.id.detail_tv);

        return view;
    }

    private void SetupTimerPicker(View view) {
        timerPicker = (LinearLayout) view.findViewById(R.id.timer_picker);
        hourPicker = (NumberPicker) view.findViewById(R.id.timer_hour_picker);
        minutePicker = (NumberPicker) view.findViewById(R.id.timer_minute_picker);
        secondPicker = (NumberPicker) view.findViewById(R.id.timer_second_picker);

        hourPicker.setMaxValue(24);
        hourPicker.setMinValue(0);
        minutePicker.setMaxValue(60);
        minutePicker.setMinValue(0);
        secondPicker.setMaxValue(60);
        secondPicker.setMinValue(0);

        hourPicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        minutePicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        secondPicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);

        hourPicker.setOnValueChangedListener(timerValueOnChangeListener);
        minutePicker.setOnValueChangedListener(timerValueOnChangeListener);
        secondPicker.setOnValueChangedListener(timerValueOnChangeListener);
    }

    NumberPicker.OnValueChangeListener timerValueOnChangeListener =  new NumberPicker.OnValueChangeListener() {
        @Override
        public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
            if (hourPicker.getValue() + minutePicker.getValue() + secondPicker.getValue() == 0) {
                leftBt.setEnabled(false);
            } else {
                leftBt.setEnabled(true);
            }
        }
    };

    private void AppendActionButtonLayout(LayoutInflater inflater, ViewGroup container, View view) {

        RelativeLayout mainLayout = (RelativeLayout) view.findViewById(R.id.timer_layout);
        LinearLayout pickerLayout = (LinearLayout) view.findViewById(R.id.timer_picker);

        RelativeLayout actionButtonView =
                (RelativeLayout) inflater.inflate(R.layout.action_button_layout, container, false);

        leftBt = (Button) actionButtonView.findViewById(R.id.left_bt);
        rightBt = (Button) actionButtonView.findViewById(R.id.right_bt);
        rightBt.setText(getResources().getString(R.string.pause_bt));
        leftBt.setOnClickListener(leftHandler);
        leftBt.setEnabled((hourPicker.getValue() + minutePicker.getValue() + secondPicker.getValue()) > 0);
        rightBt.setEnabled(false);

        RelativeLayout.LayoutParams params =
                new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE);
        params.addRule(RelativeLayout.BELOW, pickerLayout.getId());

        mainLayout.addView(actionButtonView, params);
    }

    private View.OnClickListener leftHandler = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (timerStatus) {
                case INITIAL:
                    timerStatus = TimerStatus.MESEASURING;
                    durationTime = ((hourPicker.getValue() * 60 +
                            minutePicker.getValue()) * 60 +
                            secondPicker.getValue()) * 1000;
                    startTime = SystemClock.uptimeMillis();

                    timerPicker.setVisibility(View.INVISIBLE);
                    detailTv.setVisibility(View.VISIBLE);
                    leftBt.setText(getResources().getString(R.string.cancel_bt));
                    rightBt.setEnabled(true);
                    rightBt.setOnClickListener(rightHandler);

                    watchHandler.postDelayed(updateTimerThread, 0L);
                    break;
                case MESEASURING:
                case PAUSED:
                    timerStatus = TimerStatus.INITIAL;
                    startTime = SystemClock.uptimeMillis();
                    swapTime = 0;

                    timerPicker.setVisibility(View.VISIBLE);
                    detailTv.setVisibility(View.INVISIBLE);
                    leftBt.setText(getResources().getString(R.string.start_bt));
                    rightBt.setText(getResources().getString(R.string.pause_bt));
                    rightBt.setEnabled(false);

                    watchHandler.removeCallbacks(updateTimerThread);
                    break;
                default:
                    break;
            }
        }
    };

    private View.OnClickListener rightHandler = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (timerStatus) {
                case MESEASURING:
                    timerStatus = TimerStatus.PAUSED;

                    leftBt.setText(getResources().getString(R.string.cancel_bt));
                    rightBt.setText(getResources().getString(R.string.resume_bt));

                    watchHandler.removeCallbacks(updateTimerThread);
                    break;
                case PAUSED:
                    // resume timer here
                    timerStatus = TimerStatus.MESEASURING;
                    startTime = SystemClock.uptimeMillis();
                    swapTime += timeInMilliSeconds;
                    watchHandler.removeCallbacks(updateTimerThread);

                    leftBt.setText(getResources().getString(R.string.cancel_bt));
                    rightBt.setText(getResources().getString(R.string.pause_bt));

                    watchHandler.postDelayed(updateTimerThread, 0L);
                    break;
                default:
                    break;
            }
        }
    };

    private Runnable updateTimerThread = new Runnable() {
        @Override
        public void run() {
            timeInMilliSeconds = SystemClock.uptimeMillis() - startTime;
            remainTime = durationTime - (timeInMilliSeconds + swapTime);

            if (remainTime <= 0)
            {
                timerStatus = TimerStatus.INITIAL;

                timerPicker.setVisibility(View.VISIBLE);
                detailTv.setVisibility(View.INVISIBLE);
                leftBt.setText(getResources().getString(R.string.start_bt));
                rightBt.setEnabled(false);
                ShowDoneDialog();

                watchHandler.removeCallbacks(updateTimerThread);
                return;
            }

            detailTv.setText(TimeUtil.timeToStringInSecond(remainTime));
            watchHandler.postDelayed(this, 0L);
        }
    };

    private void ShowDoneDialog() {
        Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        final Ringtone ringtone = RingtoneManager.getRingtone(getActivity().getApplicationContext(), notification);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.timer_done)
                .setMessage(R.string.timer_done)
                .setPositiveButton(R.string.timer_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ringtone.stop();
                    }
                });

        ringtone.play();
        builder.create().show();
    }
}
