package jp.co.cyberagent.mtl.timer;

/**
 * Created by a13561 on 2015/07/06.
 */
public enum TimerStatus {
    INITIAL,
    MESEASURING,
    PAUSED;
}
